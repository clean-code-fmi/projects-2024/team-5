package com.fmi.team5.myfitnesspal.command.food;

import com.fmi.team5.myfitnesspal.command.ExecutableCommand;
import com.fmi.team5.myfitnesspal.exception.InvalidCommandException;
import com.fmi.team5.myfitnesspal.food.FoodDiary;
import com.fmi.team5.myfitnesspal.food.EatingTime;
import com.fmi.team5.myfitnesspal.food.FoodId;
import com.fmi.team5.myfitnesspal.food.Food;
import com.fmi.team5.myfitnesspal.food.FoodPool;


import com.fmi.team5.myfitnesspal.constants.GlobalConstants;

import java.time.LocalDate;
import java.util.List;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.validateArgumentsCount;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseEatingTime;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseDouble;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseDate;

public final class AddFoodCommand implements ExecutableCommand {

    private static final String COMMAND_NAME = "add-food";
    private static final int ARGUMENTS_COUNT = 5;
    private final FoodDiary diary;
    private final FoodPool availableFood;

    public AddFoodCommand(FoodDiary diary, FoodPool availableFood) {
        this.diary = diary;
        this.availableFood = availableFood;
    }

    @Override
    public String execute(List<String> arguments) throws InvalidCommandException {
        validateArgumentsCount(arguments, ARGUMENTS_COUNT);

        LocalDate date = parseDate(arguments.get(0));
        EatingTime eatingTime = parseEatingTime(arguments.get(1));
        FoodId foodId = new FoodId(arguments.get(2), arguments.get(3));
        double numberOfServings = parseDouble(arguments.get(4));

        Food targetFood = this.availableFood.getFood(foodId);
        this.diary.addFood(date, eatingTime, targetFood, numberOfServings);

        return GlobalConstants.SUCCESSFULLY_ADDED_FOOD_MESSAGE;

    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public String getHelp() {
        return "Usage: " + COMMAND_NAME + " <date> <eatingTime> <foodBrand> <foodDescription> <servingCount>";
    }
}
