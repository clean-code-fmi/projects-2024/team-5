package com.fmi.team5.myfitnesspal.user;

import com.fmi.team5.myfitnesspal.user.activitylevel.ActivityLevel;
import com.fmi.team5.myfitnesspal.user.country.Country;
import com.fmi.team5.myfitnesspal.user.sex.Sex;
import com.fmi.team5.myfitnesspal.user.goal.Goal;
import com.fmi.team5.myfitnesspal.user.height.Height;
import com.fmi.team5.myfitnesspal.user.weight.Weight;

import java.util.List;

public record User(
        Height height,
        Weight weight,
        int age,
        Sex sex,
        Country country,
        ActivityLevel activityLevel,
        Weight weightGoal,
        Weight weeklyWeightGoal,
        List<Goal> goals
) {
}
