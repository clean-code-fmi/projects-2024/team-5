package com.fmi.team5.myfitnesspal.command.food;

import com.fmi.team5.myfitnesspal.command.ExecutableCommand;
import com.fmi.team5.myfitnesspal.exception.InvalidCommandException;
import com.fmi.team5.myfitnesspal.food.FoodDiary;
import com.fmi.team5.myfitnesspal.food.EatingTime;
import com.fmi.team5.myfitnesspal.food.Food;


import java.time.LocalDate;
import java.util.List;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.validateArgumentsCount;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseEatingTime;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseDate;

public final class ShowFoodsCommand implements ExecutableCommand {
    private static final String COMMAND_NAME = "show-foods";
    private static final int ARGUMENTS_COUNT = 2;
    private final FoodDiary diary;

    public ShowFoodsCommand(FoodDiary diary) {
        this.diary = diary;
    }

    @Override
    public String execute(List<String> arguments) throws InvalidCommandException {
        validateArgumentsCount(arguments, ARGUMENTS_COUNT);

        LocalDate date = parseDate(arguments.get(0));
        EatingTime eatingTime = parseEatingTime(arguments.get(1));
        StringBuilder result = new StringBuilder();
        List<Food> foods = this.diary.getFoodsByDateAndEatingTime(date, eatingTime);
        for (Food food : foods) {
            result.append(food.toString());
            result.append(System.lineSeparator());
        }

        return result.toString();
    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public String getHelp() {
        return "Usage: " + COMMAND_NAME + " <date> <eatingTime>";
    }
}
