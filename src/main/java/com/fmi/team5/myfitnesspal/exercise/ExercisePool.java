package com.fmi.team5.myfitnesspal.exercise;

import com.fmi.team5.myfitnesspal.constants.GlobalConstants;
import com.fmi.team5.myfitnesspal.exception.UnknownExerciseException;

import java.util.HashMap;
import java.util.Map;

public final class ExercisePool implements ExercisePoolAPI {
    private Map<String, Exercise> exercises = new HashMap<>();

    @Override
    public Exercise createExercise(Exercise newExercise) {
        return exercises.put(newExercise.name(), newExercise);
    }

    @Override
    public Map<String, CardioExercise> getCardioExercises() {
        Map<String, CardioExercise> toReturn = new HashMap<>();

        for (Exercise e : exercises.values()) {
            if (e instanceof CardioExercise) {
                toReturn.put(e.name(), (CardioExercise) e);
            }
        }

        return toReturn;
    }

    @Override
    public Map<String, StrengthExercise> getStrengthExercises() {
        Map<String, StrengthExercise> toReturn = new HashMap<>();

        for (Exercise e : exercises.values()) {
            if (e instanceof StrengthExercise) {
                toReturn.put(e.name(), (StrengthExercise) e);
            }
        }

        return toReturn;
    }

    @Override
    public Map<String, Workout> getWorkouts() {
        Map<String, Workout> toReturn = new HashMap<>();

        for (Exercise e : exercises.values()) {
            if (e instanceof Workout) {
                toReturn.put(e.name(), (Workout) e);
            }
        }

        return toReturn;
    }

    @Override
    public Map<String, Exercise> getExercises() {
        return exercises;
    }

    @Override
    public Exercise getExerciseByName(String name) throws UnknownExerciseException {
        Exercise exercise = exercises.get(name);

        if (exercise == null) {
            throw new UnknownExerciseException(GlobalConstants.NOT_EXISTING_EXERCISE_MESSAGE);
        }

        return exercise;
    }
}
