package com.fmi.team5.myfitnesspal.user;

public final class UserHolder {

    private User user;

    public UserHolder() {
        this.user = null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
