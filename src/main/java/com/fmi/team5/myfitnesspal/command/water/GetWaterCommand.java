package com.fmi.team5.myfitnesspal.command.water;

import com.fmi.team5.myfitnesspal.command.ExecutableCommand;
import com.fmi.team5.myfitnesspal.water.WaterDiary;
import com.fmi.team5.myfitnesspal.exception.InvalidCommandException;

import java.time.LocalDate;
import java.util.List;

import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.DATE_FORMATTER;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseDate;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.validateArgumentsCount;

public class GetWaterCommand implements ExecutableCommand {
    private static final String COMMAND_NAME = "get-water";
    private static final int ARGUMENTS_COUNT = 1;

    private final WaterDiary diary;

    public GetWaterCommand(WaterDiary diary) {
        this.diary = diary;
    }

    /**
     * Executes the command with the given arguments.
     *
     * @param arguments a list of string arguments for the command
     * @return the result of executing the command
     * @throws InvalidCommandException if the command execution fails due to invalid arguments or other reasons
     */
    @Override
    public String execute(List<String> arguments) throws InvalidCommandException {
        validateArgumentsCount(arguments, ARGUMENTS_COUNT);

        LocalDate date = parseDate(arguments.get(0));

        int water = diary.getDailyWater(date);
        return String.format("Water found for %s: %dml", date.format(DATE_FORMATTER), water);
    }

    /**
     * Returns the name of the command.
     *
     * @return the command's name as a string
     */
    @Override
    public String name() {
        return COMMAND_NAME;
    }

    /**
     * Provides help information for the command.
     *
     * @return a string containing help information about the command
     */
    @Override
    public String getHelp() {
        return "Usage: " + COMMAND_NAME + " <date>";
    }
}
