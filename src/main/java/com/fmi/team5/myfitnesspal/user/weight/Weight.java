package com.fmi.team5.myfitnesspal.user.weight;

public record Weight(int value, WeightMeasurementUnit measure) {
}
