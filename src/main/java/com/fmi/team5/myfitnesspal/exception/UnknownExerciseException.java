package com.fmi.team5.myfitnesspal.exception;

public class UnknownExerciseException extends Exception {
    public UnknownExerciseException(String message) {
        super(message);
    }
}
