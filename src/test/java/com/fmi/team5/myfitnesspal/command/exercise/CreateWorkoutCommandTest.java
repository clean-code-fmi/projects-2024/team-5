package com.fmi.team5.myfitnesspal.command.exercise;

import com.fmi.team5.myfitnesspal.constants.GlobalConstants;
import com.fmi.team5.myfitnesspal.exception.InvalidCommandException;
import com.fmi.team5.myfitnesspal.exception.UnknownExerciseException;
import com.fmi.team5.myfitnesspal.exercise.CardioExercise;
import com.fmi.team5.myfitnesspal.exercise.ExercisePool;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public final class CreateWorkoutCommandTest {
    @Mock
    ExercisePool pool;
    @InjectMocks
    CreateWorkoutCommand command;
    CardioExercise ex1 = new CardioExercise("ex1", 1, 1, LocalTime.now());
    CardioExercise ex2 = new CardioExercise("ex2", 1, 1, LocalTime.now());

    @Test
    void testExecuteValidCommand() throws InvalidCommandException, UnknownExerciseException {
        when(pool.getExerciseByName("ex1")).thenReturn(ex1);
        when(pool.getExerciseByName("ex2")).thenReturn(ex2);

        String message = command.execute(List.of("new", "ex1", "ex2"));
        assertEquals("new was created successfully!", message);
    }

    @Test
    void testExecuteLessArguments() {
        assertThrows(InvalidCommandException.class, () -> command.execute(List.of()),
            "The method should throw InvalidCommandException when the count of the arguments is 0");
    }

    @Test
    void testExecuteInvalidArguments() throws InvalidCommandException, UnknownExerciseException {
        when(pool.getExerciseByName("ex1")).thenReturn(ex1);
        when(pool.getExerciseByName("ex3")).thenThrow(UnknownExerciseException.class);

        assertEquals(GlobalConstants.NOT_EXISTING_EXERCISE_MESSAGE, command.execute(List.of("new", "ex1", "ex3")),
            "The method should throw InvalidCommandException when an exercise doesnt exist");
    }

}
