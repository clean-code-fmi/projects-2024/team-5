package com.fmi.team5.myfitnesspal.command.food;

import com.fmi.team5.myfitnesspal.command.ExecutableCommand;
import com.fmi.team5.myfitnesspal.constants.GlobalConstants;
import com.fmi.team5.myfitnesspal.exception.InvalidCommandException;
import com.fmi.team5.myfitnesspal.food.Food;
import com.fmi.team5.myfitnesspal.food.FoodId;
import com.fmi.team5.myfitnesspal.food.FoodPool;

import java.util.List;
import java.util.Optional;

import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.validateArgumentsCount;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseDoubleOptional;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseDouble;

public final class CreateFoodCommand implements ExecutableCommand {
    private static final String COMMAND_NAME = "create-food";
    private static final int ARGUMENTS_COUNT = 7;

    private final FoodPool foods;

    public CreateFoodCommand(FoodPool foods) {
        this.foods = foods;
    }

    @Override
    public String execute(List<String> arguments) throws InvalidCommandException {
        validateArgumentsCount(arguments, ARGUMENTS_COUNT);

        Food newFood = parseFood(arguments);
        this.foods.addFood(newFood);

        return GlobalConstants.SUCCESSFULLY_CREATED_FOOD_MESSAGE;
    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public String getHelp() {
        return "Usage: " + COMMAND_NAME + " <foodBrand> <foodDescription> <servingSize> "
                + "<calories> <fats> <protein> <carbs>";
    }

    private Food parseFood(List<String> arguments) throws InvalidCommandException {
        FoodId id = new FoodId(arguments.get(0), arguments.get(1));
        double servingSize = parseDouble(arguments.get(2));
        double calories = parseDouble(arguments.get(3));
        Optional<Double> fats = parseDoubleOptional(arguments.get(4));
        Optional<Double> protein = parseDoubleOptional(arguments.get(5));
        Optional<Double> carbs = parseDoubleOptional(arguments.get(6));

        return Food.builder(id, servingSize, calories)
                .setFats(fats)
                .setProtein(protein)
                .setCarbs(carbs)
                .build();
    }
}
