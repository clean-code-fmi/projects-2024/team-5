package com.fmi.team5.myfitnesspal.command;

import java.util.List;

public record Command(String command, List<String> arguments) {
}
