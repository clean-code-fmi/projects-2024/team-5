package com.fmi.team5.myfitnesspal.cli;

import com.fmi.team5.myfitnesspal.command.CommandExecutor;
import com.fmi.team5.myfitnesspal.command.CommandParser;
import com.fmi.team5.myfitnesspal.command.ExecutableCommandRegistry;
import com.fmi.team5.myfitnesspal.command.help.HelpCommand;
import com.fmi.team5.myfitnesspal.command.exercise.ShowExercisesCommand;
import com.fmi.team5.myfitnesspal.command.exercise.CreateCardioExerciseCommand;
import com.fmi.team5.myfitnesspal.command.exercise.CreateStrengthExerciseCommand;
import com.fmi.team5.myfitnesspal.command.exercise.LogExerciseCommand;
import com.fmi.team5.myfitnesspal.command.exercise.CreateWorkoutCommand;
import com.fmi.team5.myfitnesspal.command.exercise.RemoveExerciseCommand;
import com.fmi.team5.myfitnesspal.command.exercise.ShowCardioExercisesCommand;
import com.fmi.team5.myfitnesspal.command.exercise.ShowDailyExerciseCommand;
import com.fmi.team5.myfitnesspal.command.exercise.ShowStrengthExercisesCommand;
import com.fmi.team5.myfitnesspal.command.exercise.ShowWorkoutsCommand;
import com.fmi.team5.myfitnesspal.command.food.RemoveFoodCommand;
import com.fmi.team5.myfitnesspal.command.food.AddFoodCommand;
import com.fmi.team5.myfitnesspal.command.food.RemoveMealCommand;
import com.fmi.team5.myfitnesspal.command.food.AddMealCommand;
import com.fmi.team5.myfitnesspal.command.food.ShowMealsCommand;
import com.fmi.team5.myfitnesspal.command.food.ShowFoodsCommand;
import com.fmi.team5.myfitnesspal.command.food.CreateMealCommand;
import com.fmi.team5.myfitnesspal.command.food.CreateFoodCommand;
import com.fmi.team5.myfitnesspal.command.user.RegisterNewUserCommand;
import com.fmi.team5.myfitnesspal.command.water.RemoveWaterCommand;
import com.fmi.team5.myfitnesspal.command.water.RemoveWaterPortionCommand;
import com.fmi.team5.myfitnesspal.exercise.ExerciseDiary;
import com.fmi.team5.myfitnesspal.exercise.ExercisePool;
import com.fmi.team5.myfitnesspal.food.FoodDiary;
import com.fmi.team5.myfitnesspal.food.FoodPool;
import com.fmi.team5.myfitnesspal.food.MealPool;
import com.fmi.team5.myfitnesspal.registration_cli.UserRegistration;
import com.fmi.team5.myfitnesspal.user.UserHolder;
import com.fmi.team5.myfitnesspal.water.WaterDiary;
import com.fmi.team5.myfitnesspal.command.water.AddWaterCommand;
import com.fmi.team5.myfitnesspal.command.water.AddWaterPortionCommand;
import com.fmi.team5.myfitnesspal.command.water.GetWaterCommand;

import java.util.Scanner;

public final class Main {
    private Main() {
    }

    public static void main(String[] args) {
        UserHolder userHolder = new UserHolder();
        WaterDiary waterDiary = new WaterDiary();
        FoodDiary foodDiary = new FoodDiary();
        FoodPool foodPool = new FoodPool();
        MealPool mealPool = new MealPool();
        ExercisePool exercisePool = new ExercisePool();
        ExerciseDiary exerciseDiary = new ExerciseDiary(exercisePool);
        ExecutableCommandRegistry registry = new ExecutableCommandRegistry();
        CommandParser parser = new CommandParser();
        CommandExecutor executor = new CommandExecutor(registry);
        Scanner scanner = new Scanner(System.in);
        UserRegistration userRegistration = new UserRegistration(scanner);

        fillRegistry(registry, waterDiary, foodPool, foodDiary, mealPool, exercisePool, exerciseDiary, userHolder,
            scanner, userRegistration);

        Menu menu = new Menu(registry, scanner);
        menu.start();
    }

    private static void fillRegistry(ExecutableCommandRegistry registry, WaterDiary waterDiary,
                                     FoodPool foodPool, FoodDiary foodDiary, MealPool mealPool,
                                     ExercisePool exercisePool, ExerciseDiary exerciseDiary,
                                     UserHolder userHolder,
                                     Scanner scanner, UserRegistration userRegistration) {
        registry.addCommand(new AddWaterCommand(waterDiary));
        registry.addCommand(new AddWaterPortionCommand(waterDiary));
        registry.addCommand(new GetWaterCommand(waterDiary));
        registry.addCommand(new RemoveWaterCommand(waterDiary));
        registry.addCommand(new RemoveWaterPortionCommand(waterDiary));
        registry.addCommand(new CreateFoodCommand(foodPool));
        registry.addCommand(new AddFoodCommand(foodDiary, foodPool));
        registry.addCommand(new ShowFoodsCommand(foodDiary));
        registry.addCommand(new RemoveFoodCommand(foodDiary));
        registry.addCommand(new CreateMealCommand(mealPool, foodPool));
        registry.addCommand(new AddMealCommand(foodDiary, mealPool));
        registry.addCommand(new ShowMealsCommand(foodDiary));
        registry.addCommand(new RemoveMealCommand(foodDiary));
        registry.addCommand(new HelpCommand(registry));
        registry.addCommand(new RegisterNewUserCommand(scanner, userHolder, userRegistration));
        registry.addCommand(new CreateCardioExerciseCommand(exercisePool));
        registry.addCommand(new CreateStrengthExerciseCommand(exercisePool));
        registry.addCommand(new CreateWorkoutCommand(exercisePool));
        registry.addCommand(new LogExerciseCommand(exerciseDiary));
        registry.addCommand(new RemoveExerciseCommand(exerciseDiary));
        registry.addCommand(new ShowExercisesCommand(exercisePool));
        registry.addCommand(new ShowCardioExercisesCommand(exercisePool));
        registry.addCommand(new ShowDailyExerciseCommand(exerciseDiary));
        registry.addCommand(new ShowStrengthExercisesCommand(exercisePool));
        registry.addCommand(new ShowWorkoutsCommand(exercisePool));
    }
}
