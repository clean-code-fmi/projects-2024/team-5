package com.fmi.team5.myfitnesspal.constants;

public final class GlobalConstants {



    private GlobalConstants() {
    }

    public static final String NOT_VALID_ARGUMENTS_NEGATIVE_VALUE_MESSAGE = "All values should be positive!";
    public static final String NOT_EXISTING_EXERCISE_MESSAGE = "No such exercise!";
    public static final String NOT_EXISTING_FOOD_MESSAGE = "No such food in the food pool!";
    public static final String NOT_EXISTING_MEAL_MESSAGE = "No such meal in the meal pool!";
    public static final String ALREADY_ADDED_FOOD_MESSAGE = "This food was already added!";
    public static final String NOT_EXISTING_FOOD_IN_MEAL_MESSAGE = "No such food in this meal!";
    public static final String FOOD_NOT_CONSUMED_MESSAGE = "No such food eaten in this part of the day!";
    public static final String MEAL_NOT_CONSUMED_MESSAGE = "No such meal eaten in this part of the day!";
    public static final String NOT_VALID_DOUBLE_MESSAGE = "The provided number is not valid double!";
    public static final String SUCCESSFULLY_CREATED_FOOD_MESSAGE = "Food created successfully!";
    public static final String SUCCESSFULLY_ADDED_FOOD_MESSAGE = "Food added successfully!";
    public static final String NOT_EXISTING_EATING_TIME_MESSAGE = "This eating time does not exist!";
    public static final String SUCCESSFULLY_REMOVED_FOOD_MESSAGE = "Food removed successfully!";
    public static final String SUCCESSFULLY_CREATED_MEAL_MESSAGE = "Meal created successfully!";
    public static final String SUCCESSFULLY_ADDED_MEAL_MESSAGE = "Meal added successfully!";
    public static final String SUCCESSFULLY_REMOVED_MEAL_MESSAGE = "Meal removed successfully!";
    public static final String NOT_VALID_ARGUMENTS_COUNT_MESSAGE = "Arguments count not right!";
    public static final String NOT_EXISTING_DATE_IN_FOOD_DIARY_MESSAGE = "There is no such date in your food diary!";

    public static final String EMPTY_OPTIONAL_VALUE = "no";
    public static final String EXIT_COMMAND = "exit";
}
