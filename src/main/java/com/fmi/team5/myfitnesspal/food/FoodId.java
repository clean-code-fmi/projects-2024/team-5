package com.fmi.team5.myfitnesspal.food;

public record FoodId(String brand, String description) { }
