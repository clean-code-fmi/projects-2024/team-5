package com.fmi.team5.myfitnesspal.food;

import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public final class DailyFoodDiaryTest extends FoodDiaryBase {
    @Test
    public void testAddFood() {

        diary.addFood(EatingTime.BREAKFAST, firstFood, 1);

        List<Food> breakfastFoods = diary.getFoodsByEatingTime(EatingTime.BREAKFAST);

        assertEquals(1, breakfastFoods.size());
        assertEquals(breakfastFoods.get(0).getId(), new FoodId("apple", "green"));
    }

    @Test
    public void testAddMeal() {
        diary.addMeal(EatingTime.LUNCH, firstMeal);
        diary.addMeal(EatingTime.LUNCH, secondMeal);

        List<Meal> lunchMeals = diary.getMealsByEatingTime(EatingTime.LUNCH);

        assertEquals(2, lunchMeals.size());
        assertEquals(lunchMeals.get(0).getId(), new MealId("bolognese", "tomato sauce and meat"));
        assertEquals(lunchMeals.get(1).getId(), new MealId("carbonara", "cream sauce and meat"));
    }

    @Test
    public void testRemoveFood() {
        diary.addFood(EatingTime.BREAKFAST, firstFood, 1);
        diary.addFood(EatingTime.BREAKFAST, secondFood, 1);

        diary.removeFood(EatingTime.BREAKFAST, new FoodId("apple", "green"));

        List<Food> snackFoods = diary.getFoodsByEatingTime(EatingTime.BREAKFAST);

        assertEquals(1, snackFoods.size());
        assertNotEquals(snackFoods.get(0).getId(), new FoodId("apple", "green"));
        assertEquals(snackFoods.get(0).getId(), new FoodId("banana", "medium and yellow"));
    }

    @Test
    public void testRemoveMeal() {
        diary.addMeal(EatingTime.DINNER, firstMeal);
        diary.addMeal(EatingTime.DINNER, secondMeal);

        diary.removeMeal(EatingTime.DINNER, new MealId("bolognese", "tomato sauce and meat"));

        List<Meal> dinnerMeals = diary.getMealsByEatingTime(EatingTime.DINNER);

        assertEquals(1, dinnerMeals.size());
        assertNotEquals(dinnerMeals.get(0).getId(), new MealId("bolognese", "tomato sauce and meat"));
        assertEquals(dinnerMeals.get(0).getId(), new MealId("carbonara", "cream sauce and meat"));
    }

    @Test
    public void testRemoveNonExistingFood() {
        assertThrows(IllegalArgumentException.class, () -> {
            diary.removeFood(EatingTime.DINNER, new FoodId("apple", "green"));
        });
    }

    @Test
    public void testRemoveNonExistingMeal() {
        assertThrows(IllegalArgumentException.class, () -> {
            diary.removeMeal(EatingTime.DINNER, new MealId("bolognese", "tomato sauce and meat"));
        });
    }
}
