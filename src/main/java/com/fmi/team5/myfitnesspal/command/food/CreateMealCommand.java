package com.fmi.team5.myfitnesspal.command.food;

import com.fmi.team5.myfitnesspal.command.ExecutableCommand;
import com.fmi.team5.myfitnesspal.constants.GlobalConstants;
import com.fmi.team5.myfitnesspal.exception.InvalidCommandException;
import com.fmi.team5.myfitnesspal.food.FoodPool;
import com.fmi.team5.myfitnesspal.food.Food;
import com.fmi.team5.myfitnesspal.food.FoodId;
import com.fmi.team5.myfitnesspal.food.MealPool;
import com.fmi.team5.myfitnesspal.food.MealId;
import com.fmi.team5.myfitnesspal.food.Meal;


import java.util.List;

import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.validateArgumentsCount;
import static com.fmi.team5.myfitnesspal.command.utility.CommandUtilities.parseDouble;

public final class CreateMealCommand implements ExecutableCommand {
    private static final String COMMAND_NAME = "create-meal";
    private final MealPool meals;
    private final FoodPool foods;

    public CreateMealCommand(MealPool meals, FoodPool foods) {
        this.meals = meals;
        this.foods = foods;
    }

    /**
     * @param arguments a list of string in the format: mealName MealDescription brand1 description1 servings1 and so on
     */

    @Override
    public String execute(List<String> arguments) throws InvalidCommandException {
        validateArgumentsCount(arguments, (args) -> args.size() % 3 == 2);

        MealId mealId = new MealId(arguments.get(0), arguments.get(1));
        Meal resultMeal = new Meal(mealId);

        for (int i = 2; i < arguments.size() - 2; i += 3) {
            FoodId foodId = new FoodId(arguments.get(i), arguments.get(i + 1));
            double numberOfServings = parseDouble(arguments.get(i + 2));
            Food food = this.foods.getFood(foodId);
            resultMeal.addFood(food, numberOfServings);
        }

        this.meals.addMeal(resultMeal);

        return GlobalConstants.SUCCESSFULLY_CREATED_MEAL_MESSAGE;
    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public String getHelp() {
        return "Usage: " + COMMAND_NAME + " <mealName> <MealDescription> <brand1> <description1> <servings1> ...";
    }
}
