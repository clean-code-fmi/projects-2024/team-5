package com.fmi.team5.myfitnesspal.user.sex;

public enum Sex {
    MALE,
    FEMALE
}
