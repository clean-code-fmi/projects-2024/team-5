package com.fmi.team5.myfitnesspal.user.height;

public record Height(int value, LengthMeasurementUnit measure) {
}
