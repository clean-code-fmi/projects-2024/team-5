package com.fmi.team5.myfitnesspal.food;

public record MealId(String name, String description) { }
