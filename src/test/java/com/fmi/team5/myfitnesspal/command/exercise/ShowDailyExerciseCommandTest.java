package com.fmi.team5.myfitnesspal.command.exercise;

import com.fmi.team5.myfitnesspal.command.ExecutableCommand;
import com.fmi.team5.myfitnesspal.command.utility.CommandUtilities;
import com.fmi.team5.myfitnesspal.exception.InvalidCommandException;
import com.fmi.team5.myfitnesspal.exception.UnknownExerciseException;
import com.fmi.team5.myfitnesspal.exercise.CardioExercise;
import com.fmi.team5.myfitnesspal.exercise.Exercise;
import com.fmi.team5.myfitnesspal.exercise.ExerciseDiary;
import com.fmi.team5.myfitnesspal.exercise.ExercisePool;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public final class ShowDailyExerciseCommandTest {
    ExercisePool poolMock = mock(ExercisePool.class);
    ExerciseDiary diary = new ExerciseDiary(poolMock);
    ExecutableCommand command = new ShowDailyExerciseCommand(diary);
    private static final String DATE = "12.12.2012";
    CardioExercise ex1 = new CardioExercise("ex1", 1, 1, LocalTime.now());

    @Test
    void testExecuteValidCommand() throws InvalidCommandException, UnknownExerciseException {
        Map<String, Exercise> exercises = new HashMap<>();
        exercises.put("ex1", ex1);
        when(poolMock.getExercises()).thenReturn(exercises);
        when(poolMock.getExerciseByName("ex1")).thenReturn(ex1);

        diary.logExercise(CommandUtilities.parseDate(DATE), "ex1");
        String message = command.execute(List.of(DATE));
        assertEquals(ex1.toString(), message);
    }

    @Test
    void testExecuteLessArguments() {
        assertThrows(InvalidCommandException.class, () -> command.execute(List.of()),
            "The method should throw InvalidCommandException when the count of the arguments is not 1");
    }

    @Test
    void testExecuteMoreArguments() {
        assertThrows(InvalidCommandException.class,
            () -> command.execute(List.of("arg1", "arg2", "arg3")),
            "The method should throw InvalidCommandException when the count of the arguments is not 1");
    }

    @Test
    void testExecuteInvalidDate() {
        assertThrows(InvalidCommandException.class, () -> command.execute(List.of("invalid-date")),
            "The method should throw InvalidCommandException when the date is not in valid format");
    }
}
